#include <cstdio>
#include <thread>
#include <threadpool.h>
#include <set>
#include <string>
#include <cstring>
#include <chrono>
#include <dirent.h>

using namespace std;
using clock_type = chrono::high_resolution_clock;

void doSomething(string path){
    printf("%s\n", path.c_str());
}

int example_main(int argc, char const *argv[]) {
    string watchpath = "./input/";
    ThreadPool *tp = new ThreadPool(2);
    set<string> doneFiles;
    auto interval = chrono::milliseconds(500);
    auto when_started = clock_type::now(); 
    auto target_time = when_started + interval;
    vector<string> files;
    while(1){
        DIR *dir; struct dirent *diread;
        if ((dir = opendir(watchpath.c_str())) != nullptr) {
            while ((diread = readdir(dir)) != nullptr) {
                if (strcmp(diread->d_name, ".") && strcmp(diread->d_name, "..") && !(doneFiles.find(diread->d_name) != doneFiles.end())){
                    files.push_back(diread->d_name);
                    doneFiles.insert(diread->d_name);
                }
            }
            closedir (dir);
        } else {
            perror ("opendir");
            return EXIT_FAILURE;
        }
        for (auto file : files){
            tp->enqueue(doSomething, (watchpath + file));
        }
        files.clear();
        std::this_thread::sleep_until(target_time);
        target_time += interval;
    }
}



#include <stdint.h>
#include <stddef.h>
#include "sgx_utils/sgx_utils.h"
#include "Enclave_u.h"

/* Global EID (Enclave ID) could be shared by multiple threads */
sgx_enclave_id_t global_eid = 0;

int main(int argc, char const *argv[]){
    initialize_enclave(&global_eid, "enclave.token", "enclave.signed.so");
    ecall_hello(global_eid);
}
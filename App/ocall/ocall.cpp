#include "ocall.h"
#include <iostream>
#include <fstream>
#include "Enclave_u.h"
#include <byteStream.hpp>
#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>
#include <curlpp/Exception.hpp>



void uprint(const char *str) {
    /* Proxy/Bridge will check the length and null-terminate 
     * the input string to prevent buffer overflow. 
     */
    printf("ENCLAVE : %s", str);
    fflush(stdout);
}

int ocall_save_private_key(const uint8_t *sealed_data, const size_t sealed_size) {
    //TODO
    return 0;
}

int ocall_save_public_key(const char *data, const size_t size) {
    //TODO
    return 0;
}

int ocall_load_private_key(uint8_t *sealed_data, size_t sealed_size){
    //TODO
    return 0;
}

//send 
void ocall_save_output(const char *data, const size_t size){
    try {
        curlpp::Cleanup cleaner;
        curlpp::Easy request;

        std::list<std::string> headers;
        headers.push_back("Content-Type: application/octet-stream"); 

        using namespace curlpp::Options;
		request.setOpt(new Post(true));
        request.setOpt(new PostFieldSizeLarge(size));
        memstream s((uint8_t*)data, size);
        request.setOpt(new ReadStream(&s));
		request.setOpt(new HttpHeader(headers));
		request.setOpt(new Url("http://localhost:8181/update"));

        std::ofstream myfile("/dev/null");
        myfile << request;
    }
    catch ( curlpp::LogicError & e ) {
        std::cout << "LOGICERR:" << e.what() << std::endl;
    }
    catch ( curlpp::RuntimeError & e ) {
        std::cout << "RUNTIMEERR:" << e.what() << std::endl;
    }

}

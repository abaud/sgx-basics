#ifndef OCALL_H_
#define OCALL_H_
#include <stdint.h>
#include <stddef.h>
#if DEBUG
#define PRIVATE_FILE "private_key_debug.seal"
#define PUBLIC_FILE "public_key_debug.pub"
#else
#define PRIVATE_FILE "private_key.seal"
#define PUBLIC_FILE "public_key.pub"
#endif
#endif // OCALL_H_
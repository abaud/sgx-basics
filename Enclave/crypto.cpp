
#include "crypto.h"

#include <openssl/bn.h>
#include <openssl/ec.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <sgx_thread.h>
#include <trusted/sealing.h>
#include <string>
#include "Enclave_t.h"
#include "utils.h"
#include "sgx_tcrypto.h"
#include "sgx_tseal.h"


int generate_keypair(EVP_PKEY **evp_pkey){
    //EVP_PKEY allocation
    *evp_pkey = EVP_PKEY_new();
    if (evp_pkey == NULL) {
        printf("EVP_PKEY_new failure: %ld\n", ERR_get_error());
        return -1;
    }
    
    //BIGNUM allocation, will be used for the exponent of the key.
    BIGNUM *bn = BN_new();
    if (bn == NULL) {
        printf("BN_new failure: %ld\n", ERR_get_error());
        return -1;
    }

    //we use 65537 (0x10001) as the public exponent
    int ret = BN_set_word(bn, RSA_F4);
    if (!ret) {
        printf("BN_set_word failure\n\n");
        return -1;
    }

    RSA *keypair = RSA_new();
    if (keypair == NULL) {
        printf("RSA_new failure: %ld\n", ERR_get_error());
        return -1;
    }

    ret = RSA_generate_key_ex(keypair, RSA_KEY_SIZE, bn, NULL);
    if (!ret) {
        printf("RSA_generate_key_ex failure: %ld\n", ERR_get_error());
        return -1;
    }

    
    EVP_PKEY_assign_RSA(*evp_pkey, keypair);
    BN_free(bn);
    printf("RSA key pair generated ! \n");
    return 0;
}

/**
 * @brief 
 *  seal then save a private key.
 *  Sealing means encrypting such as only this enclave can read it later in this run or in a subsequent one.
 *  Using this the public key can only be shared once, and client shouldn't get the new public key 
 *  everytime the enclave is re-instanciated
 * 
 * @param evp_pkey a key pair
 */
void save_private_key(EVP_PKEY *evp_pkey){
    // private key -> string
    int len = i2d_PrivateKey(evp_pkey, NULL);
    unsigned char *buf = (unsigned char *)malloc(len + 1);
    unsigned char *p = buf;
    i2d_PrivateKey(evp_pkey, &p);
    
    //sealing
    sgx_status_t status;
    size_t sealed_size_private = sizeof(sgx_sealed_data_t) + len + 1;
    uint8_t *sealed_private_key = (uint8_t *)malloc(sealed_size_private);
    status = seal(buf, len + 1,
                  (sgx_sealed_data_t *)sealed_private_key, sealed_size_private);
    if (status != SGX_SUCCESS) {
        printf("Sealing Failed\n");
    }

    //saving to disk
    int ocall_ret;
    ocall_save_private_key(&ocall_ret, sealed_private_key, sealed_size_private);
    free(sealed_private_key);
    free(buf);
}


/**
 * @brief save public key to disk.
 * 
 * @param evp_pkey a key pair
 */
void save_public_key(EVP_PKEY *evp_pkey){
    // public key -> string
    int len = i2d_PublicKey(evp_pkey, NULL);
    unsigned char *buf = (unsigned char *)malloc(len + 1);
    unsigned char *p = buf;
    i2d_PublicKey(evp_pkey, &p);

    //saving to disk
    int ocall_ret;
    ocall_save_public_key(&ocall_ret, (const char*)buf, len);
    free(buf);
}

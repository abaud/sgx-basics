#ifndef ENCLAVE_CRYPTO_H_
#define ENCLAVE_CRPYTO_H_
#include <openssl/evp.h>

// avoid an ocall to get & malloc file size 
#define PRIVATE_KEY_SIZE 2351
#define RSA_KEY_SIZE 4096

int generate_keypair(EVP_PKEY **evp_pkey);
void save_private_key(EVP_PKEY *evp_pkey);
void save_public_key(EVP_PKEY *evp_pkey);

#endif
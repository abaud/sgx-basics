# SGX

## Installation 
### Introduction & Requirements
installing drivers, SGX sdk  and sgx-ssl. Tested for Ubuntu 20.04 (should work on Ubuntu 18.04).

requirements : 
```shell=bash
sudo apt-get install build-essential ocaml ocamlbuild automake autoconf libtool wget python-is-python3 libssl-dev git cmake perl
```

if your SGX device has `Flexible Launch Control and Intel(R) AES New Instructions support` follow **SGX Linux Driver for Intel(R) SGX DCAP**, else follow **linux-sgx-driver build and Install** instructions.

driver installation can be skipped if device has no SGX support or if you only want to run in simulation mode. 

### SGX Linux Driver for Intel(R) SGX DCAP 
The most straightforward way is to upgrade your linux kernel to >5.11, sgx drivers being part of mainline kernel since that release.
This is only available for Ubuntu 20.04, for Ubuntu 18.04 please follow instructions here : https://github.com/intel/SGXDataCenterAttestationPrimitives/blob/master/driver/linux/README.md

### linux-sgx-driver build and Install
Check if active kernel headers are installed : 
`dpkg-query -s linux-headers-$(uname -r)`
if not install those : 
`sudo apt-get install linux-headers-$(uname -r)`

clone linux-sgx-driver repo:
```shell=bash
git clone https://github.com/intel/linux-sgx-driver
cd linux-sgx-driver
git checkout sgx_driver_2.14
```
```shell=bash
make
```
Install SGX driver
```shell=bash
sudo mkdir -p "/lib/modules/"`uname -r`"/kernel/drivers/intel/sgx"    
sudo cp isgx.ko "/lib/modules/"`uname -r`"/kernel/drivers/intel/sgx"    
sudo sh -c "cat /etc/modules | grep -Fxq isgx || echo isgx >> /etc/modules"    
sudo /sbin/depmod
sudo /sbin/modprobe isgx
```

### Installing linux-sgx
preparation : 
```shell=bash
git clone https://github.com/intel/linux-sgx.git
cd linux-sgx
git checkout sgx_2.15.1
make preparation
sudo cp external/toolset/{current_distr}/{as,ld,ld.gold,objdump} /usr/local/bin
which as ld ld.gold objdump
```
Building :
```shell=bash
make sdk USE_OPT_LIBS=0
make sdk_install_pkg
```

Install:
```shell=bash
sudo ./linux/installer/bin/sgx_linux_x64_sdk_{version}.bin
# 'no' then '/opt/intel'
```

before any SGX build : 
```shell
source /opt/intel/sgxsdk/environment
```


### Installing sgx-ssl
```shell=bash
git clone https://github.com/intel/intel-sgx-ssl
cd intel-sgx-ssl
git checkout lin_2.15.1_1.1.1l
wget -P openssl_source/ https://www.openssl.org/source/openssl-1.1.1l.tar.gz
cd Linux
make all test DEBUG=1 SGX_MODE=SIM
sudo make install DEBUG=1 SGX_MODE=SIM
```


----
### repository requirements
curl & curlpp library are needed for this project : 
```shell
sudo apt install libcurl4-openssl-dev libcurlpp-dev
```

## The Basics

A SGX app will be split in 2 main parts, the untrusted, that will be the starting point of the application and will also hold anything that is not required to run inside the enclave, and the trusted one, everything that is needed to be run inside the SGX enclave.
In our example, it's visible through the file hierachy : 
```python
.
├── App # Untrusted src folder
│   ├── main.cpp # entrypoint of the application
│   ├── ocall 
│   │   ├── ocall.cpp
│   │   └── ocall.h
│   └── sgx_utils # few functions to setup and use SGX capabilities
│       ├── sgx_utils.cpp
│       └── sgx_utils.h
├── build
    ...
├── Enclave # Trusted src folder
│   ├── crypto.cpp
│   ├── crypto.h
│   ├── Enclave.config.xml # Configuration of the enclave (nbr of threads, heap size,etc.)
│   ├── Enclave.cpp
│   ├── Enclave.edl # define all trusted/untrusted functions that would trigger a switch
│   ├── private.pem # a RSA key used to sign the application in debug & simulation mode during build 
│   ├── utils.cpp
│   └── utils.h
├── include
│   ├── trusted # included during the build of the untrusted part
│   │   ├── sealing.h
│   │   ├── sgx_random.h
│   │   └── sgx_semaphore.h
│   └── untrusted # included during the build of the trusted part
│       └── threadpool.h
└── Makefile

```
### Enclave configuration
the `Enclave/Enclave.config.xml` define the enclave configuration :
```xml=
<EnclaveConfiguration>
  <ProdID>0</ProdID>
  <ISVSVN>0</ISVSVN>
  <StackMaxSize>0xF000000</StackMaxSize>
  <HeapMaxSize>0xA0000000</HeapMaxSize>
  <TCSNum>4</TCSNum>
  <TCSMinPool>4</TCSMinPool>
  <TCSPolicy>1</TCSPolicy>
  <DisableDebug>0</DisableDebug>
</EnclaveConfiguration>

```
importants fields :
- Stack Max Size : maximum stack size per thread in bytes, should be a multiple of 4KB
- Heap Max Size : Maximum Heap size for the process in bytes, should be a multiple of 4KB
- TCSNum, TCSMaxNum, TCSMinPool: Used to determine how many threads will be created after the enclave initialization, and how many threads can be created dynamically when the enclave is running.
- DisableDebug : if set to 1, the enclave can't be built and loaded in debug mode

### Initialization
before any trusted code can be run, the sgx enclave should be initialized : 
    - creation or load of a launch token. In production, this launch token should be generated by a Launch Enclave. The default one is the SGX PSW, signed by Intel and require a commercial license with Intel. Newer hardware (with SGX2) supports FLC (Flexible Launch Control) which enable the use of another Launch Enclave, enabling running enclave in production without having to go through Intel licensing and key generation verification.
    - create an enclave instance. This operation will initialize the instance and return an enclave id, used throughout the rest of the application and passed to any trusted call.
    - the launch token (if updated), will then be saved to disk for future usage.

```c=
/* Initialize the enclave:
 *   Step 1: try to retrieve the launch token saved by last transaction
 *   Step 2: call sgx_create_enclave to initialize an enclave instance
 *   Step 3: save the launch token if it is updated
 */
int initialize_enclave(sgx_enclave_id_t* eid, const std::string& launch_token_path, const std::string& enclave_name) {
    const char* token_path = launch_token_path.c_str();
    sgx_launch_token_t token = {0};
    sgx_status_t ret = SGX_ERROR_UNEXPECTED;
    int updated = 0;

    /* Step 1: try to retrieve the launch token saved by last transaction
     *         if there is no token, then create a new one.
     */
    /* try to get the token saved in $HOME */
    FILE* fp = fopen(token_path, "rb");
    if (fp == NULL && (fp = fopen(token_path, "wb")) == NULL) {
        printf("Warning: Failed to create/open the launch token file \"%s\".\n", token_path);
    }

    if (fp != NULL) {
        /* read the token from saved file */
        size_t read_num = fread(token, 1, sizeof(sgx_launch_token_t), fp);
        if (read_num != 0 && read_num != sizeof(sgx_launch_token_t)) {
            /* if token is invalid, clear the buffer */
            memset(&token, 0x0, sizeof(sgx_launch_token_t));
            printf("Warning: Invalid launch token read from \"%s\".\n", token_path);
        }
    }
    /* Step 2: call sgx_create_enclave to initialize an enclave instance */
    /* Debug Support: set 2nd parameter to 1 */
    ret = sgx_create_enclave(enclave_name.c_str(), SGX_DEBUG_FLAG, &token, &updated, eid, NULL);
    if (ret != SGX_SUCCESS) {
        print_error_message(ret);
        if (fp != NULL) fclose(fp);
        return -1;
    }

    /* Step 3: save the launch token if it is updated */
    if (updated == FALSE || fp == NULL) {
        /* if the token is not updated, or file handler is invalid, do not perform saving */
        if (fp != NULL) fclose(fp);
        return 0;
    }

    /* reopen the file with write capablity */
    fp = freopen(token_path, "wb", fp);
    if (fp == NULL) return 0;
    size_t write_num = fwrite(token, 1, sizeof(sgx_launch_token_t), fp);
    if (write_num != sizeof(sgx_launch_token_t))
        printf("Warning: Failed to save launch token to \"%s\".\n", token_path);
    fclose(fp);
    return 0;
}
```

### OCALLs and ECALLs
- OCALLs : call to an untrusted function from the trusted part of the application.
- ECALLs : a call to a trusted function from an untrusted part. wrapper functions will be generated during build to do all the SGX specifics process.

function that can be OCALLs & ECALLs are defined inside .edl file, in our example inside `Enclave/Enclave.edl`

```edl=
/* enclave.edl - Top EDL file. 
    define all the ecall, or trusted function : call that will run inside the enclave 
    and the ocall (untrusted), function that run outside the enclave and can be call from within.
*/

enclave {
    from "sgx_tstdc.edl" import *;
    from "sgx_pthread.edl" import *;

    /* 
     *  [in]: copy the string buffer to App outside.
     *  [string]: specifies 'str' is a NULL terminated buffer.
     */
    untrusted {
        void uprint([in, string] const char *str);
        int ocall_save_private_key(
            [in, size=sealed_size]const uint8_t *sealed_data,
            size_t sealed_size
        );
        int ocall_save_public_key(
            [in, size=size]const char *data,
            size_t size
        );
        int ocall_load_private_key(
            [out, size=sealed_size]uint8_t *sealed_data,
            size_t sealed_size
        );
    };
    
    trusted {
        public void ecall_hello(); 
        public void ecall_generate_rsa_key();
    };
};
```

this file can import other edl file, either from other project (here sgx_tstdc.edl & sgx_pthread.edl are part of the sgx SDK).
a function signature should be included in either the `untrusted` or `trusted` block. If a pointer is passed in argument, definition of the pointed data should be included:
- how it will be used : 
    - in : the data is copied from the callers memory to the callee memory on call
    - out : a buffer will be created on call, and all data written to that buffer will be copied to the original (caller's side) buffer upon return.
    - in,out : combine in & out, buffer will be copied to the callee memory and back to the caller's one upon return.
- size of the buffer : 
    - size=y : y being a argument of the function with the size (in bytes) of the buffer. will be used by the wrappers functions to copy the right buffer.
    - string : the passed pointer is a c string and is null terminated.

trusted functions can either be public, and can be called directly by the App, or private, can't be directly called by the App.

### First ECALL
let's implement our first ecall. 
```cpp=
int ecall_double(int i){
    k = 2*i;
    return k;
}
```
once this function defined inside `Enclave.cpp`, we need to add it's signature to `Enclave.edl`.
```edl=
enclave{
    untrusted{
    
    };
    trusted{
        public int ecall_double(int i);
    };
};
```
at this point, building the project (`make SGX_MODE=SIM` or just `make` for hardware debug mode) will generate `build/generated/Enclave_{t,u}.{h,c}`. 
Those file contains the code and definitions of ecalls & ocalls, with the switch between the trusted and untrusted part of the memory.
In the untrusted part of the app, to call our ecall_double function we need to call the function defined inside the `enclave_u.h` file : 
```cpp=
sgx_status_t ecall_double(sgx_enclave_id_t eid, int i);
```
the eid is the enclave eid, which is set during the initializatio of the enclave instance. i is the argument of our ecall_double function.

in `main.cpp` to make a ecall we do : 
```cpp=
#include "Enclave_u.h"

/* Global EID (Enclave ID) could be shared by multiple threads */
sgx_enclave_id_t global_eid = 0;

int main(int argc, char const *argv[]){
    initialize_enclave(&global_eid, "enclave.token", "enclave.signed.so");
    int ret;
    ecall_double(global_eid, &ret, 4);
    printf("%d\n", ret);
}
```
### First OCALL
we'll now write a ocall that let us write to console output from inside the enclave code.
in the untrusted part (inside `App/ocall/ocall.cpp`) we define `uprint` : 
```cpp=
void uprint(const char *str) {
    /* Proxy/Bridge will check the length and null-terminate 
     * the input string to prevent buffer overflow. 
     */
    printf("ENCLAVE : %s", str);
    fflush(stdout);
}
```
we now define it inside the untrusted part of the `Enclave.edl` : 
```edl=
enclave{
    untrusted{
        void uprint([in, string] const char *str);
    };
    trusted{
        public void ecall_double(int i);
    };
};
```
the proxy to this function inside the enclave is defined in `Enclave_t.h`, its signature is the same.
inside `Enclave/utils.cpp` let's define a `printf` replacement that uses `uprint` to write to the terminal : 
```cpp=
#include "Enclave_t.h"

/* 
 * printf: 
 *   Invokes OCALL to display the enclave buffer to the terminal.
 */
void printf(const char *fmt, ...) {
    char buf[BUFSIZ] = {'\0'};
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);
    uprint(buf);
}
```

we can now change `ecall_double` to write the result back to the terminal : 
```cpp=
#include "utils.h"
#include "Enclave_t.h"

int ecall_double(int i){
    k = 2*i;
    printf("k=%d", k);
    return k;
}

```

### Ecall using a buffer
Write an ECALL that takes a string representing an array of 5 integers, parse it and write the sum in a file, through an OCALL.
This exercice relies on the good understanding of the EDL format to accurately defines the pointer passed through those ECALL & OCALL.

## Crypto inside the Enclave
SGX SDK can, if installed, feature SGXSSL which is based on the openSSL source code. This mean that, with some caveat concerning threading, cryptographic work inside a SGX enclave is done the same way as an openSSL project.

### RSA keys generation
First, let's generate a RSA key pair inside the enclave. We'll then save the public key to the disk without encryption and the private key will be sealed and then written on disk. This way the enclave can then load and unseal the private key for later uses, even in the case of the enclave being shutdown.
Sealing is done using a private key that is unique to the platform & the enclave, meaning that only the right enclave ran on the specific hardware can unseal the data.

Using OpenSSL documentation, the generation of a RSA key pair : 
>
 ```cpp=
#include "crypto.h"

#include <openssl/bn.h>
#include <openssl/ec.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <sgx_thread.h>
#include "Enclave_t.h"
#include "utils.h"
#include "sgx_tcrypto.h"
#include "sgx_tseal.h"


int generate_keypair(EVP_PKEY **evp_pkey){
    //EVP_PKEY allocation
    *evp_pkey = EVP_PKEY_new();
    if (evp_pkey == NULL) {
        printf("EVP_PKEY_new failure: %ld\n", ERR_get_error());
        return -1;
    }
    
    //BIGNUM allocation, will be used for the exponent of the key.
    BIGNUM *bn = BN_new();
    if (bn == NULL) {
        printf("BN_new failure: %ld\n", ERR_get_error());
        return -1;
    }

    //we use 65537 (0x10001) as the public exponent
    int ret = BN_set_word(bn, RSA_F4);
    if (!ret) {
        printf("BN_set_word failure\n\n");
        return -1;
    }

    RSA *keypair = RSA_new();
    if (keypair == NULL) {
        printf("RSA_new failure: %ld\n", ERR_get_error());
        return -1;
    }

    ret = RSA_generate_key_ex(keypair, RSA_KEY_SIZE, bn, NULL);
    if (!ret) {
        printf("RSA_generate_key_ex failure: %ld\n", ERR_get_error());
        return -1;
    }

    
    EVP_PKEY_assign_RSA(*evp_pkey, keypair);
    BN_free(bn);
    printf("RSA key pair generated ! \n");
    return 0;
}
 ```
> note : in a real use-case, the printf-s to provide details on errors or success of the generation should be removed as it reduce performances because of the necessary ocall to write to the terminal.

 the related ecall and signature inside the enclave definition : 
```cpp=
#include "crypto.h"

void ecall_generate_rsa_key(){
    EVP_PKEY *pkey;
    generate_keypair(&pkey);
}
```
```edl=
enclave{
    untrusted{
        void uprint([in, string] const char *str);
    };
    trusted{
        public void ecall_generate_rsa_key();
        public void ecall_double(int i);
    };
};
```

running the enclave at this point would generate a key pair. The sealing is a process entirely supported by the SDK, function for sealing/unsealing are available in `include/trusted/Sealing.hpp` : 
```cpp=
#pragma once
#ifndef TRUSTED_SEALING_H_
#define TRUSTED_SEALING_H_

#include "sgx_trts.h"
#include "sgx_tseal.h"

/**
 * @brief      Seals the plaintext given into the sgx_sealed_data_t structure
 *             given.
 *
 * @details    The plaintext can be any data. uint8_t is used to represent a
 *             byte. The sealed size can be determined by computing
 *             sizeof(sgx_sealed_data_t) + plaintext_len, since it is using
 *             AES-GCM which preserves length of plaintext. The size needs to be
 *             specified, otherwise SGX will assume the size to be just
 *             sizeof(sgx_sealed_data_t), not taking into account the sealed
 *             payload.
 *
 * @param      plaintext      The data to be sealed
 * @param[in]  plaintext_len  The plaintext length
 * @param      sealed_data    The pointer to the sealed data structure
 * @param[in]  sealed_size    The size of the sealed data structure supplied
 *
 * @return     Truthy if seal successful, falsy otherwise.
 */
inline sgx_status_t seal(unsigned char* plaintext, size_t plaintext_len, sgx_sealed_data_t* sealed_data, size_t sealed_size) {
    sgx_status_t status = sgx_seal_data(0, NULL, plaintext_len, (uint8_t*)plaintext, sealed_size, sealed_data);
    return status;
}

/**
 * @brief      Unseal the sealed_data given into c-string
 *
 * @details    The resulting plaintext is of type uint8_t to represent a byte.
 *             The sizes/length of pointers need to be specified, otherwise SGX
 *             will assume a count of 1 for all pointers.
 *
 * @param      sealed_data        The sealed data
 * @param[in]  sealed_size        The size of the sealed data
 * @param      plaintext          A pointer to buffer to store the plaintext
 * @param[in]  plaintext_max_len  The size of buffer prepared to store the
 *                                plaintext
 *
 * @return     Truthy if unseal successful, falsy otherwise.
 */
inline sgx_status_t unseal(sgx_sealed_data_t* sealed_data, size_t sealed_size,unsigned char* plaintext, uint32_t plaintext_len) {
    sgx_status_t status = sgx_unseal_data(sealed_data, NULL, NULL, (uint8_t*)plaintext, &plaintext_len);
    return status;
}

#endif
```

We can, using those, seal the private key : 
```cpp
    // private key -> string
    int len = i2d_PrivateKey(evp_pkey, NULL);
    unsigned char *buf = (unsigned char *)malloc(len + 1);
    i2d_PrivateKey(evp_pkey, &buf);
    
    //sealing
    sgx_status_t status;
    size_t sealed_size_private = sizeof(sgx_sealed_data_t) + len + 1;
    uint8_t *sealed_private_key = (uint8_t *)malloc(sealed_size_private);
    status = seal(buf, len + 1,
                  (sgx_sealed_data_t *)sealed_private_key, 
                  sealed_size_private);
    if (status != SGX_SUCCESS) {
        printf("Sealing Failed\n");
    }
```

Right now, the key pair and the sealed data are only stored inside the enclave memory, and we need to send those to the untrusted part of the app to access the filesystem and save them on disk. Let's define OCALLs for saving & loading of private & public keys : 
```cpp
#include <iostream>
#include <fstream>

int ocall_save_private_key(const uint8_t *sealed_data, const size_t sealed_size) {
    std::ofstream file(PRIVATE_FILE, std::ios::out | std::ios::binary);
    if (file.fail()) {
        return -1;
    }
    file.write((const char *)sealed_data, sealed_size);
    file.close();
    return 0;
}

int ocall_save_public_key(const char *data, const size_t size) {
    std::fstream file(PUBLIC_FILE, std::ios::out | std::ios::binary);
    if (file.fail()) {
        return -1;
    }
    file.write((const char *)data, size);
    file.close();
    return 0;
}


```

```edl
enclave{
    untrusted{
        void uprint([in, string] const char *str);
        int ocall_save_private_key(
            [in, size=sealed_size]const uint8_t *sealed_data,
            size_t sealed_size
        );
        int ocall_save_public_key(
            [in, size=size]const char *data,
            size_t size
        );
    };
    trusted{
        public void ecall_generate_rsa_key();
        public void ecall_double(int i);
    };
};
```

Now we can change our ECALL to : 
- generate a key pair
- seal the private key
- save the public key to disk
- save the sealed private key to disk

inside `Enclave/crypto.cpp`, we define 2 functions to save the keys: 
```cpp=
#include "crypto.h"

#include <openssl/bn.h>
#include <openssl/ec.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/evp.h>
#include <sgx_thread.h>
#include <trusted/sealing.h>
#include <string>
#include "Enclave_t.h"
#include "utils.h"
#include "sgx_tcrypto.h"
#include "sgx_tseal.h"

/**
 * @brief 
 *  seal then save a private key.
 *  Sealing means encrypting such as only this enclave can read it later in this run or in a subsequent one.
 *  Using this the public key can only be shared once, and client shouldn't get the new public key 
 *  everytime the enclave is re-instanciated
 * 
 * @param evp_pkey a key pair
 */
void save_private_key(EVP_PKEY *evp_pkey){
    // private key -> string
    int len = i2d_PrivateKey(evp_pkey, NULL);
    printf("PRIVATE_KEY_SIZE : %d", len); //used later when loading private key
    unsigned char *buf = (unsigned char *)malloc(len + 1);
    i2d_PrivateKey(evp_pkey, &buf);
    
    //sealing
    sgx_status_t status;
    size_t sealed_size_private = sizeof(sgx_sealed_data_t) + len + 1;
    uint8_t *sealed_private_key = (uint8_t *)malloc(sealed_size_private);
    status = seal(buf, len + 1,
                  (sgx_sealed_data_t *)sealed_private_key, sealed_size_private);
    if (status != SGX_SUCCESS) {
        printf("Sealing Failed\n");
    }

    //saving to disk
    int ocall_ret;
    ocall_save_private_key(&ocall_ret, sealed_private_key, sealed_size_private);
    free(buf);
}


/**
 * @brief save public key to disk.
 * 
 * @param evp_pkey a key pair
 */
void save_public_key(EVP_PKEY *evp_pkey){
    // public key -> string
    int len = i2d_PublicKey(evp_pkey, NULL);
    unsigned char *buf = (unsigned char *)malloc(len + 1);
    i2d_PublicKey(evp_pkey, &buf);

    //saving to disk
    int ocall_ret;
    ocall_save_public_key(&ocall_ret, (const char*)buf, len);
    free(buf);
}
```
We change our previous ECALL to save the generated keys :
```cpp
void ecall_generate_rsa_key(){
    EVP_PKEY *pkey;
    generate_keypair(&pkey);
    save_private_key(pkey);
    save_public_key(pkey);
}
```
The saved public key can now be used to encrypt data outside the enclave. To make more usable with other tools, we can change its format (to PEM for example) using OpenSSL CLI tool : 
```bash
openssl rsa -RSAPublicKey_in -in public_key_debug.pub -inform DER -outform PEM -out pubkey.pem
```

Now we can also define an OCALL & ECALL that just load & unseal the private key and store it inside the enclave memory for later uses:
- ocall to load the private key file and passing its content to trusted memory : 
```cpp=vector
int ocall_load_private_key(uint8_t *sealed_data, size_t sealed_size){
    std::ifstream file(PRIVATE_FILE, std::ios::in | std::ios::binary);
    if (file.fail()) {
        return -1;
    }
    file.read((char *)sealed_data, sealed_size);
    file.close();
    return 0;
}
```

- the private key loading function and creation of the evp_pkey & RSA using the loaded key (inside `crypto.cpp`) : 
```cpp
//to avoid an unnecessary ocall, we store private key size as a constant
#define PRIVATE_KEY_SIZE 2351

void load_private_key(unsigned char **pkey){
    size_t sealed_size = PRIVATE_KEY_SIZE + sizeof(sgx_sealed_data_t);
    sgx_status_t ocall_status;
    uint8_t *sealed_data = (uint8_t *)malloc(sealed_size);
    int ocall_ret;
    unsigned char *iv, *enckey, *cipher, *tag, *plaintext;
    int ivlen, enckeylen, cipherlen, taglen;
    int plaintextlen;
    sgx_status_t sealing_status;
    uint32_t private_len = PRIVATE_KEY_SIZE;
    unsigned char *private_key = (unsigned char *)malloc(private_len);
    ocall_status = ocall_load_private_key(&ocall_ret, sealed_data, sealed_size);
    if (ocall_ret != 0 || ocall_status != SGX_SUCCESS) {
        free(sealed_data);
        return;
    }
    sealing_status = unseal((sgx_sealed_data_t *)sealed_data,
                            sealed_size, 
                            private_key, 
                            private_len);
    free(sealed_data);
    if (sealing_status != SGX_SUCCESS) {
        free(private_key);
        return;
    }
    *pkey = private_key;
    printf("%ld", private_len);
}

void create_evp_pkey(EVP_PKEY **evp_pkey, const unsigned char *pkey){
    *evp_pkey = d2i_PrivateKey(EVP_PKEY_RSA, NULL, &pkey, PRIVATE_KEY_SIZE);
}

void create_rsa_privatekey(RSA **rsa, const unsigned char *pkey){
    d2i_RSAPrivateKey(rsa, &pkey, PRIVATE_KEY_SIZE);
}

```
- calling `load_private_key` and storing key to memory for futur usages : 
```cpp=
EVP_PKEY *evp_pkey;

void ecall_init(){
    unsigned char *pkey;
    load_private_key(&pkey);
    create_evp_pkey(&evp_pkey, pkey);
}
```

### Exercises
load the public key inside the Enclave (using either an OCALL or loading it through the argument of a ECALL).
Then, using `RSA * d2i_RSAPublicKey(RSA **a, const unsigned char **pp, long length);` create a RSA public key and use it to encrypt a small message provided as an argument of the ECALL, then encode it using base64 (functions are provided in `base64.h` in the `include` folder) to then write it to a file.
Write an ECALL taking a base64 encrypted message, unbase64 then decrypting it using the private key.
here's basic implementation of the encrypt/decrypt part : 
```cpp=
int public_encrypt(unsigned char * data,int data_len,RSA* rsa, unsigned char *encrypted)
{
    int result = RSA_public_encrypt(data_len,data,encrypted,rsa,padding);
    return result;
}

int private_decrypt(unsigned char * enc_data,int data_len,RSA *rsa, unsigned char *decrypted)
{
    int  result = RSA_private_decrypt(data_len,enc_data,decrypted,rsa,padding);
    return result;
}
```

### EVP Authenticated Encryption & Decryption
SGXSSL being based on openSSL, implementing AEAD is done in the same way inside the enclave as in a normal untrusted application. For Mixnn I used this Openssl documentation : https://wiki.openssl.org/index.php/EVP_Authenticated_Encryption_and_Decryption


## Threading
multi-threading inside the enclave is possible. The enclave configuration need to specify the number (max/min) of threads allowed to run in the enclave.
Creating multiple threads in the untrusted part of the app and executing ECALLs on each of those will result in multiple threads inside the enclave.
Synchronization might be necessary for some application, the SGX SDK provides such as mutex or condition.
sgx mutex example : 
```c=
#include <sgx_thread.h>
int count = 0;
// sgx mutex need to be initialized, this macro does this for you.
sgx_thread_mutex_t mutex = SGX_THREAD_MUTEX_INITIALIZER; 

void thread_fun(){
    sgx_thread_mutex_lock(&mutex);
    count++;
    sgx_thread_mutex_unlock(&mutex);
}
```

For more complex synchronization, semaphore might be needed, an implementation of Semaphore using `sgx_thread.h` is provided in `include/trusted/sgx_semaphore.h` of the sgx_basics repo : 
```c=
#ifndef SEMAPHORE_TRUSTED_H_
#define SEMAPHORE_TRUSTED_H_

#include <sgx_thread.h>

class Semaphore {
    sgx_thread_mutex_t mutex_;
    sgx_thread_cond_t condition_;
    unsigned long count_ = 0; // Initialized as locked.

public:
    inline Semaphore(){}
    inline Semaphore(unsigned long count){
        this->count_ = count;
        this->mutex_ = SGX_THREAD_MUTEX_INITIALIZER;
        this->condition_ = SGX_THREAD_COND_INITIALIZER;
    }

    inline ~Semaphore(){
        sgx_thread_cond_destroy(&this->condition_);
        sgx_thread_mutex_destroy(&this->mutex_);
    }
    inline void release() {
        sgx_thread_mutex_lock(&mutex_);
        ++count_;
        sgx_thread_cond_signal(&condition_);
        sgx_thread_mutex_unlock(&mutex_);
    }

    inline void acquire() {
        sgx_thread_mutex_lock(&mutex_);
        while(!count_) // Handle spurious wake-ups.
            sgx_thread_cond_wait(&condition_, &mutex_);
        --count_;
        sgx_thread_mutex_unlock(&mutex_);
    }

    inline bool try_acquire() {
        if(count_) {
            --count_;
            return true;
        }
        return false;
    }
};

#endif

```

Also to facilitate threading usage, a threadpool (`threadpool.h`) implementation is provided inside the untrusted include folder.

### Mixnn: implementing Mixing 
In this part we'll implement the basic data structure needed for mixnn and the actual mixing part using threading and synchronization.
We won't implement the crypto nor use actual neural network weights data structures to keep it simple.
We need to define 5 lists (or pools) of integer. Once enough integers were added to those list, we randomly take 1 (and remove it from the list) from each list, those 5 integers represent our mixed update.
The implemented pool should follow those specifications : 
- can hold a specific number of element (size of the array)
- synchronization should be done, such as 2 thread trying to add (or remove) an integer won't collide. Semaphores & using 2 lists of empty/filled space inside the array could be helpful for that.
- can return the number of integer it's holding.
- can return 1 random number of the list, removing it at the same time.
write this data structure using `sgx_semaphore.h`, `sgx_thread_mutex` & `sgx_random.h` (provided in `./include/trusted/sgx_random.h`)
Once this done, test it by writing an ECALL taking 5 integer as argument and adding it to each list, and retrieving 5 random one if a certain threshold is hit. Then test it with multiple threads adding integers.

### Mixnn : combining with crypto
Now we will combine the previous exercices together:
The main thread will watch for new files inside an `input` directory. Those files will contains array of 5 integers, encrypted using the RSA key generated previously (and encoded in base64 for easier string manipulation).
you will load the content of each file, pass it to the enclave to decrypt, then add each one inside one of the previously implemented list.
Once the threshold is hit inside those list, use the already implemented `ocall_save_output` ocall to send the result to a http server.
an example to watch a directory for new file is given in `App/dir_watch_ex.cpp`.
the http server is written in go and available in `server/server.go`, to test the application you can start it : 
```shell
go run server/server.go -p 8181
```


## Performances
Timing performances inside an enclave is not a straight-forward process, and it's simpler and more accurate to do the timing inside the untrusted part as it will also measure the context-switching and memory encryption/decryption needed by each ECALLs.

### Timing an ECALL
Since we're doing timing outside the enclave, classic way of timing an execution can be done : 
```cpp=
#include <chrono>
/* other necessary includes */

sgx_enclave_id_t global_eid = 0;
int main(){
    using std::chrono::high_resolution_clock;
    using std::chrono::duration_cast;
    using std::chrono::duration;
    using std::chrono::milliseconds;
    
    if (initialize_enclave(global_eid, "enclave.token", "enclave.signed.so") < 0){
        printf("Err: Couldn't initialize enclave.\n");
        return 1;
    }
    auto t1 = high_resolution_clock::now();
    ecall_generate_rsa_key(global_eid);
    auto t2 = high_resolution_clock::now();
    
    /* Getting number of milliseconds as an integer. */
    auto ms_int = duration_cast<milliseconds>(t2 - t1);

    std::cout << ms_int.count() << "ms\n";
    return 0;
}
```


### Using AB to test all the workflow
For Mixnn, the enclave act as a proxy. The workflow used in our implementation is as such : 
- Client send it's encrypted data to the proxy
- the proxy save the data to disk
- the untrusted part of our App watch the filesystem for new clients data. When a new file is found, it's loaded and passed through an ecall to the enclave.
- the enclave decrypts the data and stores it inside its memory
- when enough data is received by the enclave, a process (the mixing) is done to the data, the resulting data is then (through an OCALL) sent to the target server.

To time the performance of all this workflow for increasing number of clients, we use AB (Apache Bench) and time how long it takes between the first data sent and when the last processed data reach the target server.

`perftest.py` is small utility that use ab to time all this workflow. it requires `colorama` & `fire` was tested on python 3.9.

all the testing workflow : 
- run the input server on port 8081 (will write to `./input/` folder) : `go run server/server.go -p 8081`
- run the output server on port 8181, redirecting stdout to a file (used by `perftest.py`) : `go run server/server.go -p 8181 >> ./target.log`
- with the sgx app and the previously written function, generate an encrypted message that contains an array of 5 integers (parseable by the rest of the sgx app). this file will be located at `./message.enc` in the rest of this explanation 
-  run the sgx app : `./app`
-  start perftest.py : 
    ```shell 
    python perftest.py --payload_file "./message.enc" --input_server "http://localhost:8081/upload" --output_logfile "./target.log" --nb_req 1000 --nb_expected 990
    ```
    `nb_req` is the number of requests ab will send to the input server.
    `nb_expected` is the number of requests expected on the output server. Without preloading the lists, `nb_req - nb_expected = threshold`, i.e. how many requests is needed before the sgx app starts mixing. 
    if the lists are filled before starting the test, `nb_req` & `nb_expected` will be equal

#ifndef TRUSTED_RAND_H_
#define TRUSTED_RAND_H_

#include <inttypes.h>
#include <stdlib.h>
#include <openssl/rand.h>

inline int FastLog2(int x) {
    float fx;
    unsigned long ix, exp;

    fx = (float)x;
    ix = *(unsigned long *)&fx;  // Flag for time of evil hacking.
    exp = (ix >> 23) & 0xFF;

    return exp - 127;
}

inline unsigned int random_int(int min, int max) {
    int diff = max - min;
    if (diff <= 0) {
        return min;  // not random
    }
    int range = diff + 1;
    int bits = FastLog2(range);
    int bytesize = bits / 8 + (bits % 8 != 0);  // fast ceil(x/y)
    unsigned int divider = 1 << bits;
    unsigned char *randbytes = (unsigned char *)malloc(bytesize);
    unsigned int value;
    do {
        RAND_priv_bytes(randbytes, bytesize);
        value = 0;
        for (int i = 0; i < bytesize; i++) {
            value = value << 8;
            value += (unsigned int)randbytes[i];
        }
        value %= divider;
        if (value < range) {
            break;
        }
    } while (1);
    free(randbytes);
    return value + min;
}

#endif
import time
import os
import fire
from threading import Thread
from itertools import zip_longest
import sys
from colorama import Fore, Back, Style
import datetime
import subprocess
import math

def current_time():
    return datetime.datetime.now().strftime('%H:%M:%S.%f')[:-3]

def print_info(tag, message):
    print(f"{Fore.LIGHTWHITE_EX}[{current_time()}]{Fore.RESET} [{Fore.GREEN}INFO{Fore.RESET}][{tag}]\t{message}")

def print_error(tag, message):
    print(f"{Fore.LIGHTWHITE_EX}[{current_time()}]{Fore.RESET}[{Fore.RED}ERROR{Fore.RESET}][{tag}]\t{message}")

def is_finished():
    return (ab_finished and (g_nb_received >= g_nb_expected)) or g_stop

def follow(paths, callbacks=sys.stdout.write, sleep_duration=1, end_check=is_finished) : 
    ''' 
    follow (as in tail --follow) a list of text files.
    each line is redirected to a callback function

    Arguments:
        files - list of of File to follow
        callbacks - callback function or list of callback 
        sleep_duration - sleep duration between 2 file read, in seconds
    '''
    files = []
    for idx, path in enumerate(paths):
        callback = None
        if type(callbacks) is list:
            callback = callbacks[idx]
        else:
            callback = callbacks
        files.append((open(path), callback))

    for file, _ in files:
        file.seek(0,os.SEEK_END)

    read = False
    while True:
        for file, callback in files:
            curr_position = file.tell()
            line = file.readline()
            if not line:
                file.seek(curr_position)
            else :
                callback(line.strip("\n"))
                read = True
        if not read :
            time.sleep(1)
        if end_check() :
            print_info("LOG", "every action terminated, exiting now.")
            break
        read = False


def thread_ab(nb_req, address, payload) :
    global g_start_time
    proc = subprocess.Popen(f"ab -c 1 -n {nb_req} -p {payload} {address}".split(), stdout=subprocess.PIPE, stderr= subprocess.STDOUT)
    g_start_time = math.trunc(time.time() * 1000)
    for line in proc.stdout :
        ab_parser(line.decode().strip())
    return

def thread_logger(output_logfile) :
    follow([output_logfile], callbacks=[server_parser])

def ab_parser(line):
    global ab_finished
    if "Time taken for tests:" in line :
        ab_finished = True
        time = line.split(":")[-1].strip()
        print_info("AB", f"all requests sent in {time} !")
    elif "(be patient)" in line :
        print_info("AB","ab start...")

def server_parser(line):
    global g_nb_received
    global g_last_received
    prefix, message = line.split("\t")
    if "ERROR" in prefix :
        print_error("SERVER", f"{message}") 
    if "INFO" in prefix :
        if "Update received" in message:
            g_nb_received += 1
            g_last_received = [int(i) for i in message.split() if i.isdigit()][0]
            if(g_nb_received > 0 and g_nb_received%g_log_step == 0) : 
                print_info("SERVER", f"{g_nb_received} updates received\t({(g_nb_received/g_nb_expected*100):.0f}%)")

        else:
            print_info("SERVER", f"{message}")

def perfTest(payload_file="payload.post" ,input_server="http://localhost:8181/update",output_logfile="./target.log", nb_req=1000, nb_expected=990, log_step=None, timeout=60) :
    # global vars setup
    global g_nb_expected
    global ab_finished
    global g_last_received
    global g_stop
    g_nb_expected = nb_expected
    ab_finished = False
    g_last_received = 0
    g_stop = False
    
    if not log_step :
        log_step = math.ceil(nb_expected/10)
    global g_log_step
    g_log_step = log_step

    tlog = Thread(target=thread_logger, args = (output_logfile,))
    tlog.start()
    tab = Thread(target=thread_ab, args = (nb_req, input_server, payload_file))
    tab.start()
    while True:
        time.sleep(10)
        if is_finished() :
            break
        now = time.time() * 1000
        if g_last_received > 0 and (now - g_last_received)/1000 > timeout :
            print_error("MAIN", "timeout, target server didn't received enough response in time.")
            g_stop = True
            break
    tlog.join()
    tab.join()
    print_info("STATS", f"sent {nb_req} requests, received {g_nb_received} (expected : {nb_expected}) in {(g_last_received - g_start_time)} ms.")


if __name__ == '__main__':
    global g_nb_received
    g_nb_received = 0
    fire.Fire(perfTest)


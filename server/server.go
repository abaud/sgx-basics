package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"
)

const MAX = 4

var (
	InfoLogger  *log.Logger
	ErrorLogger *log.Logger
	sem         chan int
)

func init() {
	sem = make(chan int, MAX)
	colorReset := "\033[0m"
	colorRed := "\033[31m"
	colorGreen := "\033[32m"
	InfoLogger = log.New(os.Stdout, string(colorGreen)+"INFO"+string(colorReset)+":\t", log.Ltime|log.Lmsgprefix)
	ErrorLogger = log.New(os.Stdout, string(colorRed)+"ERROR"+string(colorReset)+":\t", log.Ltime|log.Lmsgprefix)
}

func main() {
	port := flag.Int("p", 8181, "Port to Listen to.")
	flag.Parse()

	http.HandleFunc("/upload", HelloServer)
	http.HandleFunc("/update", Update)
	InfoLogger.Println("Started Server on localhost:" + strconv.Itoa(*port))
	ErrorLogger.Fatal(http.ListenAndServe(":"+strconv.Itoa(*port), nil))
}

func Update(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		now := time.Now()
		InfoLogger.Println("Update received @ ", now.UnixMilli(), " Content-Length: ", r.ContentLength)

		w.WriteHeader(200)
		fmt.Fprintf(w, "Successfully Uploaded File\n")
		break
	default:
		ErrorLogger.Println("received an unallowed method for /upload")
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

func HelloServer(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodPost:
		now := time.Now()
		dst, err := os.Create("input/" + strconv.FormatInt(now.UnixMilli(), 10) + ".bin")
		defer dst.Close()

		n, err := io.Copy(dst, r.Body)
		InfoLogger.Println("Read ", n)
		if err != nil {
			ErrorLogger.Println("Error Write")
			ErrorLogger.Println(err)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		w.WriteHeader(200)
		fmt.Fprintf(w, "Successfully Uploaded File\n")
		break
	default:
		ErrorLogger.Println("received an unallowed method for /upload")
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
	}
}

